/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.algorithm;

/**
 *
 * @author Lori Machado Filho
 */
import br.edu.utils.Particle;
import br.edu.utils.FileUtils;
import br.edu.utils.HungarianBipartiteMatching;
import java.util.*;
import ij.*;
import ij.gui.*;
import ij.measure.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

//computer assisted sperm analysis plugin, based on mTrack2r
public class GNN implements Measurements {

    private JFrame parent;

    ImagePlus imp;
    int nParticles;
    float[][] ssx;
    float[][] ssy;
    String directory, filename;

    float gating = 10f;
    int cyclesToCalibrateKalmanFilter = 5;
    float kalmanMaximumDistance = 150f;
    float minimumAppearanceRate = 100f;
    int beginCycle = 5;

    public GNN() {

    }

    public GNN(JFrame parent) {
        this.parent = parent;
    }

    public void run(String file) {
        //the stuff below is the box that pops up to ask for pertinant
        //values - why doesn't it remember the values entered????

        GenericDialog gd = new GenericDialog("Sperm Tracker");

        // Gating (circle)
        gd.addNumericField("a, Gating", gating, 10, 20, "");
        //Zero because the number os frames is low
        gd.addNumericField("b, Cycles to Calibrate Kalman Filter?", cyclesToCalibrateKalmanFilter, 5, 20, "");
        //It is a great distance because there is only 100 frames, so the kalman filter may not be calibrated enough
        gd.addNumericField("c, Kalman Maximum Distance", kalmanMaximumDistance, 150, 20, ""); 
        //Minimum rate to consider a sperm track. In case the sperm apears in less frames than this, it will not be saved at the export file
        gd.addNumericField("d, Minimum Appearance Rate", minimumAppearanceRate, 100, 20, ""); 
        //Attribute needed to calibrate the Kalman filter. The algorithm will start considering the kalman filter after the X iteration. X = BeginCycle
        gd.addNumericField("e, Begin Cycle", beginCycle, 5, 20, ""); 
        gd.showDialog();
        if (gd.wasCanceled()) {
            return;
        }

        gating = (float) gd.getNextNumber();
        cyclesToCalibrateKalmanFilter = (int) gd.getNextNumber();
        kalmanMaximumDistance = (float) gd.getNextNumber();
        minimumAppearanceRate = (float) gd.getNextNumber();
        beginCycle = (int) gd.getNextNumber();

        trackWithCSV(file);
    }

    public double myAngle(double dX, double dY) {
        double aRad = 0;
        double aFinal = 0;
        if (dY > 0) {
            //gives us 0->90, the angle is fine
            if (dX > 0) {
                aRad = Math.atan((dY) / (dX));
                aFinal = ((aRad * 180) / Math.PI);
            } else if (dX < 0) //gives us 0->-90, we need to add 180 (90->180)
            {
                aRad = Math.atan((dY) / (dX));
                aFinal = 180 + ((aRad * 180) / Math.PI);
            } else if (dX == 0) {
                aFinal = 90;
            }
        } else if (dY < 0) {
            //gives us 0->-90, add 360 and we've got it (270->360)
            if (dX > 0) {
                aRad = Math.atan((dY) / (dX));
                aFinal = 360 + ((aRad * 180) / Math.PI);
            } else if (dX < 0) //gives us 0->90, add 180 and we've got it (180->270)
            {
                aRad = Math.atan((dY) / (dX));
                aFinal = 180 + ((aRad * 180) / Math.PI);
            } else if (dX == 0) //should be able to handle -90 becomes 270 in the new system
            {
                aFinal = 270;
            }
        } else if (dY == 0) {
            if (dX > 0) {
                aFinal = 0;
            } else if (dX < 0) {
                aFinal = 180;
            }
        }
        return aFinal;
    }

    //Angle change and Direction Change; this may represent a substitute for beat cross calculations, however, I have not determined its usefullness and it does appear to be highly dependent on frame rate
    public double[] myAngleDelta(double oldA, double storA, double directionVCL) {
        //aChange is element 0
        //directionVCL is element 1
        //0 is clockwise, 1 is counter-clockwise
        double arrayAngle[] = new double[2];
        //use the angle from above to calculate the change from the previous angle (all angles are relative to the axis...)
        double holdA1 = oldA + 180;
        double holdA2 = oldA - 180;

        if (oldA > 180) {
            if (holdA2 < storA && oldA > storA) {
                arrayAngle[0] = oldA - storA;
                arrayAngle[1] = 0;
            } else if (oldA < storA) {
                arrayAngle[0] = storA - oldA;
                arrayAngle[1] = 1;
            } else if (oldA > storA) {
                arrayAngle[0] = storA - oldA + 360;
                arrayAngle[1] = 1;
            } else if (oldA == storA) {
                arrayAngle[0] = 0;
                arrayAngle[1] = directionVCL;
            } else if (holdA2 == storA) {
                arrayAngle[0] = 0;
                arrayAngle[1] = directionVCL;
            }
        } else if (oldA <= 180) {
            if (holdA1 > storA && oldA < storA) {
                arrayAngle[0] = storA - oldA;
                arrayAngle[1] = 1;
            } else if (oldA > storA) {
                arrayAngle[0] = oldA - storA;
                arrayAngle[1] = 0;
            } else if (oldA < storA) {
                arrayAngle[0] = oldA - storA + 360;
                arrayAngle[1] = 0;
            } else if (oldA == storA) {
                arrayAngle[0] = 0;
                arrayAngle[1] = directionVCL;
            } else if (holdA1 == storA) {
                arrayAngle[0] = 0;
                arrayAngle[1] = directionVCL;
            }
        }
        return arrayAngle;
    }

    public void trackWithCSV(String file) {
        try {
            if(minimumAppearanceRate>100){
                JOptionPane.showMessageDialog(parent,"Minimum rate greater than 100. The algorithm will not find any sperm.");
                return;
            }
            FileUtils fileUtils = new FileUtils();
            //Load file
            try {
                fileUtils.carregarArquivo(file, cyclesToCalibrateKalmanFilter);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(parent, "Error loading CSV file. \n" + ex.getMessage());
                Logger.getLogger(GNN.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
            int nFrames = fileUtils.getQtdFrames();
            if (nFrames < 2) {
                JOptionPane.showMessageDialog(parent, "Stack required");
                return;
            }

            // create storage for particle positions
            ArrayList[] theParticles = new ArrayList[nFrames];
            int trackCount = 0;

            // record particle positions for each frame in an ArrayList
            theParticles = fileUtils.getTheParticles();

            // now assemble tracks out of the particle lists
            // Also record to which track a particle belongs in ArrayLists
            List theTracks = new ArrayList();
            for (int i = 0; i <= (nFrames - 1); i++) {
                IJ.showProgress((double) i / nFrames);
                for (ListIterator j = theParticles[i].listIterator(); j.hasNext();) {
                    Particle aParticle = (Particle) j.next();
                    if (!aParticle.inTrack) {
                        // This must be the beginning of a new track
                        List aTrack = new ArrayList();
                        trackCount++;
                        aParticle.inTrack = true;
                        aParticle.trackNr = trackCount;
                        aTrack.add(aParticle);
                        // search in next frames for more particles to be added to track
                        boolean searchOn = true;
                        Particle oldParticle = new Particle();
                        Particle tmpParticle = new Particle();
                        oldParticle.copy(aParticle);
                        //Creat lists for Hungarian algorithm 
                        //row = estimated positons
                        //column = detected positions
                        ArrayList<Particle> estimatedPositions = new ArrayList();
                        ArrayList<Particle> detectedPositions = new ArrayList();
                        
                        for (int iF = i + 1; iF <= (nFrames - 1); iF++) {
                            boolean foundOne = false;
                            Particle newParticle = new Particle();
//                            Particle kalmanParticle = new Particle();
                            float beginCycleAux =  beginCycle;
                            for (ListIterator jF = theParticles[iF].listIterator(); jF.hasNext() && searchOn;) {
                                Particle testParticle = (Particle) jF.next();
                                float distance = testParticle.distance(oldParticle);
                                if (distance < gating) {
                                    // record a particle when it is within the search radius, and when it had not yet been claimed by another track
                                    testParticle.copyKalman((Particle) aTrack.get(aTrack.size() - 1));
                                    //Do predict and correct from kalman filter 
                                    testParticle.PredictAndCorrect(testParticle.x, testParticle.y, testParticle.x - oldParticle.x, testParticle.y - oldParticle.y);                                    
                                    //get kalman distance (next predicted motion speed)
                                    Float kalmanDistance = testParticle.getKalmanDistancePredict();
                                    if ((!testParticle.inTrack && kalmanDistance != null && kalmanDistance > 0 && kalmanDistance < kalmanMaximumDistance)
                                            || beginCycleAux > 0) {
                                        beginCycleAux--;
                                        //found? so take it to the list for assignment using hungarian algorithm later on 
                                        estimatedPositions.add(testParticle.getEstimatedParticle());
                                        detectedPositions.add(testParticle.getDetectedParticle());
                                        
                                        // if we had not found a particle before, it is easy
                                        if (!foundOne) {
                                            tmpParticle = testParticle;
                                            testParticle.inTrack = true;
                                            testParticle.trackNr = trackCount;
                                            newParticle.copy(testParticle);
                                            foundOne = true;
                                        } else {
                                            // if we had one before, we'll take this one if it is closer.  In any case, flag these particles
                                            testParticle.flag = true;
                                            if (distance < newParticle.distance(oldParticle)) {
                                                testParticle.inTrack = true;
                                                testParticle.trackNr = trackCount;
                                                newParticle.copy(testParticle);
                                                tmpParticle.inTrack = false;
                                                tmpParticle.trackNr = 0;
                                                tmpParticle = testParticle;
                                            } else {
                                                //Else? set flag true, we do not want this particle
                                                newParticle.flag = true;
                                            }
                                        }
                                    } else if (kalmanDistance != null && kalmanDistance > 0 && kalmanDistance < gating) {
                                        testParticle.flag = true;
                                    }
                                }
                            }

                            if (foundOne) {
                                aTrack.add(newParticle);
                            } else {
                                searchOn = false;
                            }
                            oldParticle.copy(newParticle);
                        }
                        
                        //Hungarian Algorithm here
                        //Enter only if the cost matrix has the minium size (greater or equas to the positions found)
                        if (!estimatedPositions.isEmpty() && !detectedPositions.isEmpty() 
                                && estimatedPositions.size() <= aTrack.size()
                                && detectedPositions.size() <= aTrack.size()) {
                            double[][] costMatrix = new double[estimatedPositions.size()][detectedPositions.size()];
                            
                            //Transform the ArrayList structure in native matrix structure
                            for (int row = 0; row < estimatedPositions.size(); row++) {
                                for (int column = 0; column < detectedPositions.size(); column++) {
                                    costMatrix[row][column] = (double) estimatedPositions.get(row).distance(detectedPositions.get(column));
                                }
                            }
                            //Initiate Hungariam algorithm using the cost matrix (euclidian distance between estimated positions and detected positions)
                            HungarianBipartiteMatching hbm = new HungarianBipartiteMatching(costMatrix);
                            //run it
                            int[] result = hbm.execute();
                            ArrayList<Particle> newTrack = new ArrayList();
                            //Refresh the track
                            for (int k = 0; k < result.length; k++) {
                                newTrack.add((Particle) aTrack.get(result[k]));
                            }                            
                            // put it in the track list
                            theTracks.add(newTrack);
                        } else {
                            theTracks.add(aTrack);
                        }
                    }
                }
            }
            fileUtils.setTheParticles(theParticles);
            fileUtils.setMinimumAppearanceRate(minimumAppearanceRate);
            fileUtils.exportarArquivo();

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(parent, "Unknown Error. See log.");
            Logger.getLogger(GNN.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }

    }

    // Utility functions
    double sqr(double n) {
        return n * n;
    }

    int doOffset(int center, int maxSize, int displacement) {
        if ((center - displacement) < 2 * displacement) {
            return (center + 4 * displacement);
        } else {
            return (center - displacement);
        }
    }

    double s2d(String s) {
        Double d;
        try {
            d = new Double(s);
        } catch (NumberFormatException e) {
            d = null;
        }
        if (d != null) {
            return (d.doubleValue());
        } else {
            return (0.0);
        }
    }

}
