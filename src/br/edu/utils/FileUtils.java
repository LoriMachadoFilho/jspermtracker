/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utils;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Lori Machado Filho
 */
public class FileUtils {
    private ArrayList[] theParticles = null;
    private Integer qtdFrames;
    private JFrame parent;
    private static Integer frame;
    private static float minimumAppearanceRate;

    private static class Coordenada extends Point {

        public double x;
        public double y;

        public Coordenada(double x, double y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public Object clone() {
            return super.clone();
        }
    }

    public static HashMap<Integer, Coordenada> mapaCoordenadas;

    public void run(String arquivo) {
        try {
            carregarArquivo(arquivo, 10);

            for (Map.Entry<Integer, Coordenada> entry
                    : mapaCoordenadas.entrySet()) {
                Integer key = entry.getKey();
                Coordenada value = entry.getValue();
                System.out.println(value.x + ", " + value.y);
            }
        } catch (Exception ex) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            String sStackTrace = sw.toString();
            JOptionPane.showMessageDialog(parent, "Houve um problema na realização do Algoritmo GNN.\nErro " + sStackTrace);
        }

    }

    /**
     * Arquivo a ser lido. O formato deve seguir como FRAME, COORDENADA X e
     * COORDENADA Y. Separados por ; em um arquivo .CSV.
     *
     * @param arquivo
     * @throws Exception
     */
    public void carregarArquivo(String arquivo, int cyclesToCalibrateKalmanFilter) throws Exception {
        
        System.out.println("Lendo arquivo...");
        File file = new File(arquivo);

        if (!file.exists()) {
            throw new Exception("Arquivo (" + arquivo + ") não existe.");
        }

        if (!arquivo.endsWith(".csv")) {
            throw new Exception("Somente arquivos com extensão CSV são permitidos. ");
        }

        BufferedReader reader = new BufferedReader(new FileReader(file));
        String linha = "";
        linha = reader.readLine();
        if (!linha.split(";")[0].matches("[\\D]*")) {
            throw new Exception("A primeira linha deve ser o cabeçalho.");
        }
   
        // lê o arquivo  
        reader = new BufferedReader(new FileReader(file));
        reader.readLine(); //Cabeçalho
        int frameAux = -1;
        frame = 0;
        Particle particle = null;
        
        //Lê o arquivo apenas para ver a quantidade de frames
        while ((linha = reader.readLine()) != null) {
             String[] split = linha.split(";");
            frame = Integer.valueOf(split[0]);            
            if(frame != frameAux){
                System.out.println("Lendo Frame: " + frame);
                frameAux = frame;
            }
        }
        frame++; //O frame no arquivo começa em Zero, então +1 para contar esse valor
        qtdFrames = frame;
        theParticles = new ArrayList[qtdFrames];
        int i =0;
        for (ArrayList theParticle : theParticles) {
            theParticle = new ArrayList();
            theParticles[i++] = theParticle;
        }
        
        //Agora sim lê para armazenar as informações necessárias
        reader = new BufferedReader(new FileReader(file));
        reader.readLine(); //Cabeçalho
        frameAux = -1;
        frame = 0;
        while ((linha = reader.readLine()) != null) {
            i = 0;
            String[] split = linha.split(";");
            frame = Integer.valueOf(split[i++]);            
            if(frame != frameAux){
                System.out.println("Lendo Frame: " + frame);
                frameAux = frame;
            }
            Float x = Float.valueOf(split[i++].replace(",", "."));
            Float y = Float.valueOf(split[i++].replace(",", "."));
     
            particle = new Particle(x, y);
            particle.cyclesToCalibrateKalmanFilter = cyclesToCalibrateKalmanFilter;
            particle.z = frame;
            theParticles[frame].add(particle);
        }
        System.out.println("Populou a Matriz com as coordenadas do arquivo");

        
    }
    
    /*
     * Arquivo a ser lido. O formato deve seguir como FRAME, COORDENADA X e
     * COORDENADA Y. Separados por ; em um arquivo .CSV.
     *
     * @param arquivo
     * @throws Exception
     */
    public Integer[][] carregarArquivoComRetorno(String arquivo, int cyclesToCalibrateKalmanFilter) throws Exception {
        File file = new File(arquivo);

        if (!file.exists()) {
            throw new Exception("Arquivo (" + arquivo + ") não existe.");
        }

        if (!arquivo.endsWith(".csv")) {
            throw new Exception(
                    "Somente arquivos com extensão CSV são permitidos. ");
        }

        BufferedReader reader = new BufferedReader(new FileReader(file));
        String linha = "";
        mapaCoordenadas = new HashMap();
        Integer identificador = 0;
        linha = reader.readLine();
        if (!linha.split(";")[0].matches("[\\D]*")) {
            throw new Exception("A primeira linha deve ser o cabeçalho.");
        }
        //Lê a primeira vez para ver quantas casas existem após a vírgula
        Integer casasDecimais = 0;
        Integer casasDecimaisAux = 0;
        Integer maiorNumeroInteiro = 0;
        Integer maiorNumeroInteiroAux = 0;
        while ((linha = reader.readLine()) != null) {
            try {
                maiorNumeroInteiroAux = Double.valueOf(linha.split(";")[1].replace(",", ".")).intValue();
                if (maiorNumeroInteiroAux > maiorNumeroInteiro) {
                    maiorNumeroInteiro = maiorNumeroInteiroAux;
                }
                maiorNumeroInteiroAux = Double.valueOf(linha.split(";")[2].replace(",", ".")).intValue();
                if (maiorNumeroInteiroAux > maiorNumeroInteiro) {
                    maiorNumeroInteiro = maiorNumeroInteiroAux;
                }

                casasDecimaisAux = linha.split(";")[1].split(",")[1].length();
                if (casasDecimaisAux > casasDecimais) {
                    casasDecimais = casasDecimaisAux;
                }
                casasDecimaisAux = linha.split(";")[2].split(",")[1].length();
                if (casasDecimaisAux > casasDecimais) {
                    casasDecimais = casasDecimaisAux;
                }
            } catch (Exception ex) {
                //Cai na Exception quando o número não tem casas após a virgula
            }
        }
        System.out.println("Maior número de casas decimais: " + casasDecimais);
        System.out.println("Maior número inteiro: " + maiorNumeroInteiro);
//         int casas = (String.valueOf(maiorNumeroInteiro).length() + casasDecimais);
        String tamanhoMatriz = "";
        for (int i = 0; i < maiorNumeroInteiro.toString().length() + 1; i++) {
            tamanhoMatriz += "9";
        }
        System.out.println("Maior tamanho da matriz: " + tamanhoMatriz);
        String patern = "0."; //Uma casaa decimail, caso contrário irá ocupar muita memória desnecessária
        for (int i = 0; i < 1; i++) {
            patern += "0";
        }
        DecimalFormat dc = new DecimalFormat(patern);
        System.out.println("teste decimal: " + dc.format(new Integer(54)));
        System.out.println("teste decimal: " + dc.format(new Double(54.578)));
        System.out.println("teste decimal: " + dc.format(new Double(54.57887815158)));

        // agora le o arquivo e armazena em uma matriz com o tamanho otimizado baseado nos valores lidos anteriormente
        System.out.println("Armazenando memória para a matriz: " + (1l * Integer.SIZE * Integer.valueOf(tamanhoMatriz) * Integer.valueOf(tamanhoMatriz)) / 8 / 1024 / 1024 + "Mb");
        Integer[][] matriz = new Integer[Integer.valueOf(tamanhoMatriz)][Integer.valueOf(tamanhoMatriz)];

        //Popula a matriz com 0;
        for (int i = 0; i < Integer.valueOf(tamanhoMatriz); i++) {
            for (int j = 0; j < Integer.valueOf(tamanhoMatriz); j++) {
                matriz[i][j] = 0;
            }
        }
        System.out.println("Populou a matriz com zeros");
        reader = new BufferedReader(new FileReader(file));
        reader.readLine(); //Cabeçalho
        int frameAux = -1;
        int frame = 0;
        Particle particle = null;
        
        //Lê o arquivo apenas para ver a quantidade de frames
        while ((linha = reader.readLine()) != null) {
             String[] split = linha.split(";");
            frame = Integer.valueOf(split[0]);            
            if(frame != frameAux){
                System.out.println("Lendo Frame: " + frame);
                frameAux = frame;
            }
        }
        frame++; //O frame no arquivo começa em Zero, então +1 para contar esse valor
        qtdFrames = frame;
        theParticles = new ArrayList[qtdFrames];
        int i =0;
        for (ArrayList theParticle : theParticles) {
            theParticle = new ArrayList();
            theParticles[i++] = theParticle;
        }
        
        //Agora sim lê para armazenar as informações necessárias
        reader = new BufferedReader(new FileReader(file));
        reader.readLine(); //Cabeçalho
        frameAux = -1;
        frame = 0;
        while ((linha = reader.readLine()) != null) {
            i = 0;
            String[] split = linha.split(";");
            frame = Integer.valueOf(split[i++]);            
            if(frame != frameAux){
                System.out.println("Lendo Frame: " + frame);
                frameAux = frame;
            }
            Double x = Double.valueOf(split[i++].replace(",", "."));
            Double y = Double.valueOf(split[i++].replace(",", "."));
            x = Double.valueOf(dc.format(x).replace(",", "."));
            y = Double.valueOf(dc.format(y).replace(",", "."));
            
            
            mapaCoordenadas.put(identificador, new Coordenada(x, y));

            Integer xI = Integer.valueOf(String.valueOf(x).replace(".", ""));
            Integer yI = Integer.valueOf(String.valueOf(y).replace(".", ""));

            particle = new Particle(xI.floatValue(), yI.floatValue());
            particle.cyclesToCalibrateKalmanFilter = cyclesToCalibrateKalmanFilter;
            particle.z = frame;
            theParticles[frame].add(particle);
            matriz[xI][yI] = 1;
        }
        System.out.println("Populou a Matriz com as coordenadas do arquivo");

        
        return matriz;
    }
    
    public void exportarArquivo() throws Exception {
        File file = new File("files/Saída_GNN_" +  System.currentTimeMillis() +".csv");
        file.createNewFile();
        
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.append("Frame;x;y;Sperm;Distance");
        writer.newLine();
        ArrayList<Particle> listaOrdenada = new ArrayList();
        
        //Passa para uma lista única
        System.out.println("Passando para lista única...");
        for (ArrayList theParticle : theParticles) {
            for (Object object : theParticle) {
                listaOrdenada.add((Particle) object);
            }
        }
        
        //Ordena lista única
        System.out.println("Ordenando Sperm...");
        Collections.sort(listaOrdenada, new Comparator<Particle>() {
            @Override
            public int compare(Particle t, Particle t1) {
                return new Integer(t.z).compareTo(t1.z);
            }
        });
        Collections.sort(listaOrdenada, new Comparator<Particle>() {
            @Override
            public int compare(Particle t, Particle t1) {
                return new Integer(t.trackNr).compareTo(t1.trackNr);
            }
        });
        
        System.out.println("Retirando espermas que não estão em todos os Frames...");
        //Retira os espermatozóides que não estão 100% na amostra, isto é, que não estão em todos os frames
        int idSperm = 0, idSpermAux = 0;
        ArrayList<Particle> listaParaRemover = new ArrayList();
        ArrayList<Particle> listaAux = new ArrayList();
        for (Particle particle : listaOrdenada) {
            idSpermAux = particle.trackNr;
            if(idSperm != idSpermAux){
                idSperm = idSpermAux;
                if(listaAux.size() < ((frame)*(minimumAppearanceRate/100))){
                    for (Particle particle1 : listaAux) {
                        listaParaRemover.add(particle1);                        
                    }
                    listaAux.clear();
                } else {
                    listaAux.clear();
                }
            }
            listaAux.add(particle);
        }        
        if(listaAux.size() == 1){
            listaParaRemover.add(listaAux.get(0));            
        }
        for (Particle particle : listaParaRemover) {
            listaOrdenada.remove(particle);
        }
              
        
        //Exporta lista unica ordenada conforme ID sperm
        System.out.println("Exportando...");
        Particle particleAux = new Particle(0, 0);
        for (Particle particle : listaOrdenada) {
            writer.append(particle.z+";"+Float.toString(particle.x).replace(".", ",")+";"+Float.toString(particle.y).replace(".", ",")+";"+particle.trackNr+";"+particle.distance(particleAux));
            writer.newLine();
            particleAux = particle;
        }
        writer.close();
        
        System.out.println("Arquivo exportado");
    }
    
    public Integer getQtdFrames(){
        return this.qtdFrames;
    }
    
    public ArrayList[] getTheParticles(){
        return this.theParticles;
    }
    
    public void setTheParticles(ArrayList[] theParticles){
        this.theParticles = theParticles;
    }

    public static float getMinimumAppearanceRate() {
        return minimumAppearanceRate;
    }

    public static void setMinimumAppearanceRate(float minimumAppearanceRate) {
        FileUtils.minimumAppearanceRate = minimumAppearanceRate;
    }    
    
}
