/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utils;

import jama.Matrix;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import jkalman.JKalman;

/**
 *
 * @author Lori Machado Filho
 */
public class Particle {

    public Particle() {
        Random rand = new Random(System.currentTimeMillis() % 2011);
        init(rand.nextFloat(), rand.nextFloat(), rand.nextDouble(), rand.nextDouble());
    }
    public Particle(float x, float y) {
        Random rand = new Random(System.currentTimeMillis() % 2011);
        init(x,y, rand.nextDouble(),rand.nextDouble());
    }
    public Particle(float x, float y, double dx, double dy) {
        init(x,y, dx, dy);       
    }
    
    public void init(float x, float y, double dx, double dy){
        try {
            kalman = new JKalman(4, 2);
        } catch (Exception ex) {            
            Logger.getLogger(Particle.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }

//        Random rand = new Random(System.currentTimeMillis() % 2011);
        this.x = x;
        this.y = y;
        // constant velocity
        this.dx = dx;
        this.dy = dy;

        // init
        s = new Matrix(4, 1); // state [x, y, dx, dy, dxy]        
        c = new Matrix(4, 1); // corrected state [x, y, dx, dy, dxy]                

        m = new Matrix(2, 1); // measurement [x]
        m.set(0, 0, x);
        m.set(1, 0, y);

        // transitions for x, y, dx, dy
        double[][] tr = {{1, 0, 1, 0},
                         {0, 1, 0, 1},
                         {0, 0, 1, 0},
                         {0, 0, 0, 1}};
        kalman.setTransition_matrix(new Matrix(tr));

        // 1s somewhere?
        kalman.setError_cov_post(kalman.getError_cov_post().identity());
        
        // init first assumption similar to first observation (cheat :)
        // kalman.setState_post(kalman.getState_post());
    }
    
    public void PredictAndCorrect(double x, double y, double dx, double dy) {
        // check state before
        s = kalman.Predict();

        // function init :)
        // m.set(1, 0, rand.nextDouble());
//        x = rand.nextGaussian();
//        y = rand.nextGaussian();

        m.set(0, 0, m.get(0, 0) + dx /*+ rand.nextGaussian()*/);
        m.set(1, 0, m.get(1, 0) + dy /*+ rand.nextGaussian()*/);

        // measurement is ok :)
        // look better
        c = kalman.Correct(m);

    }
        
    
    public JKalman kalman;
    public Matrix s;
    public Matrix c;
    public Matrix m;
    public double dx;
    public double dy;
    public int cyclesToCalibrateKalmanFilter;

    public float x;
    public float y;
    public int z;
    public int trackNr;
    public boolean inTrack = false;
    public boolean flag = false;

    public void copy(Particle source) {
        this.x = source.x;
        this.y = source.y;
        this.z = source.z;
        this.inTrack = source.inTrack;
        this.flag = source.flag;
        this.kalman = source.kalman;
        this.s = source.s;
        this.c = source.c;
        this.m = source.m;
        this.dx = source.dx;
        this.dy = source.dy;
        this.cyclesToCalibrateKalmanFilter = source.cyclesToCalibrateKalmanFilter;
//        init(x, y, dx, dy);
    }
    public void copyKalman(Particle source) {
        this.kalman = source.kalman;
        this.s = source.s;
        this.c = source.c;
        this.m = source.m;
        this.dx = source.dx;
        this.dy = source.dy;
        this.cyclesToCalibrateKalmanFilter = source.cyclesToCalibrateKalmanFilter;        
    }

    public Float getKalmanDistancePredict() {
        if(cyclesToCalibrateKalmanFilter<=0){
            return (float) Math.sqrt(Math.pow((this.x - s.get(0, 0)),2) + Math.pow((this.y - s.get(1,0)),2));
        } else {
            cyclesToCalibrateKalmanFilter--;
            return 0f;
        }
    }
    
    public Particle getEstimatedParticle(){
        return new Particle((float)s.get(0, 0), (float)s.get(1, 0));
    }
    
    public Particle getDetectedParticle(){
        return new Particle(this.x, this.y);
    }
    public float distance(Particle p) {
        return (float) Math.sqrt(Math.pow((this.x - p.x),2) + Math.pow((this.y - p.y),2));
    }
    
    public Matrix getStateMatrix(){
        return this.s;
    }
    
    public Matrix getCorrectStateMatrix(){
        return this.c;
    }

}
