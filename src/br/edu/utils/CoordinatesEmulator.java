/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utils;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lori Machado Filho
 */
public class CoordinatesEmulator extends Applet {

    int width, height;
    public Integer[][] matriz = null;

    @Override
    public void init() {

        width = getSize().width;
        height = getSize().height;
        setBackground(Color.black);
        try {
            FileUtils fileUtils = new FileUtils();
            matriz = fileUtils.carregarArquivoComRetorno("files\\Entrada do Tracking.csv", 10);
        } catch (Exception ex) {
            Logger.getLogger(CoordinatesEmulator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void paint(Graphics g) {

        g.setColor(Color.green);

        for (int j = 0; j < 9999; j++) {
            for (int k = 0; k < 9999; k++) {
                if (matriz[j][k] == 1) {
                    g.fillOval(j / 10, k / 10, 10, 10);
                    
                }
            }
        }

    }

}
